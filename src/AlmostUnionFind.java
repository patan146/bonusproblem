import reader.Reader;

import java.io.IOException;
import java.util.*;

/**
 * User: Patrik
 * Date: 2013-10-09
 * Time: 19:21
 */
public class AlmostUnionFind {
    public static void main(String[] args) throws IOException {
        new AlmostUnionFind().run();
    }

    int n;
    int m;
    Node[] nodes;

    private void run() throws IOException {
        Reader in = new Reader();
        do {
            int[] line = in.readInts();
            n = line[0];
            m = line[1];
            nodes = new Node[n];

            for (int i = 0; i < n; i++) {
                nodes[i] = new Node(i);
            }

            for (int i = 0; i < m; i++) {
                line = in.readInts();
                int command = line[0];
                int a, b;
                switch (command) {
                    case 1://Union
                        a = line[1] - 1;//rank-1 = index
                        b = line[2] - 1;
                        union(nodes[a], nodes[b]);
                        break;
                    case 2://Move
                        a = line[1] - 1;
                        b = line[2] - 1;
                        move(nodes[a], nodes[b]);
                        break;
                    case 3://Count
                        a = line[1] - 1;
                        count(nodes[a]);
                        System.out.println(countNr + " " + countSum);
                        break;
                    case 4://print sets
                        printSets();
                        break;
                    case 5://remove
                        a = line[1] - 1;
                        extractToEmptySet(nodes[a]);
                        break;
                }
            }
        } while (in.ready());
    }

    void union(Node small, Node big) {
        Node topBig = getTop(big);
        Node topSmall = getTop(small);
        if (topSmall != topBig) {
            topSmall.parent = topBig;
            topBig.children.add(topSmall);//add topSmall as child
            //update values of topBig
            topBig.nrOfChildrenAndThis += topSmall.nrOfChildrenAndThis;
            topBig.sum += topSmall.sum;
        }
    }

    void extractToEmptySet2(final Node node) {
        /*
        Remove trace of node from parents.
        Do some repointing while at it :)
        Sets all nodes on the way to top as children to top.
        Adds all values to top except those of node and it's children.
         */
        List<Node> walkers = new LinkedList<>();
        Node walker = node;
        while (walker.parent.parent != null) {//while walker.parent isn't top
            walkers.add(walker.parent);
            walker.parent.sum -= walker.sum;
            walker.parent.nrOfChildrenAndThis -= walker.nrOfChildrenAndThis;
            walker = walker.parent;
        }
        for (Node superChild : walkers) {
            superChild.parent = walker;//now top
            walker.sum += superChild.sum;
            walker.nrOfChildrenAndThis += superChild.nrOfChildrenAndThis;
        }
        if (walker == node) {//if node is top
            //replace node with a child
            Iterator<Node> it = node.children.iterator();
            if (it.hasNext()) {
                walker = it.next();
                walker.sum -= node.index + 1;//index+1=rank
                walker.nrOfChildrenAndThis--;
                node.parent = walker;
                while (it.hasNext()) {
                    Node child = it.next();
                    child.parent = walker;
                    walker.sum += child.sum;
                    walker.nrOfChildrenAndThis += child.nrOfChildrenAndThis;
                }
                walker.children = new HashSet<>();
            } else {//if there is no child, node is already alone
                return;
            }
        }
        //Move children to walker aka top
        for (Node child: node.children){
            child.parent = walker;
            walker.sum+= child.sum;
            walker.nrOfChildrenAndThis += child.nrOfChildrenAndThis;
        }
        node.parent=null;
        node.children = new HashSet<>();
    }

    void extractToEmptySet(final Node node) {
        if (node.parent == null) {
            if (node.children.isEmpty()) return;
            else {//replace node with replacement(a child) for node's children
                Iterator<Node> it = node.children.iterator();
                Node replacement = it.next();
                replacement.children.add(node);//add node as child to replacement
                node.parent = replacement;//set parent of node to replacement
                replacement.parent = null;//set replacement as a top
                it.remove();//remove replacement as child of node
                for (Node child : node.children) {//add children of node to replacement
                    child.parent = null;
                    union(child, replacement);
                }
                //remove node from the set
                node.parent = null;
                node.sum = node.index + 1;
                node.nrOfChildrenAndThis = 1;
                node.children = new HashSet<>();
                //add node to replacement
                union(node, replacement);
            }
            //Now node has a parent
            //remove only the values of node(not it's children) from parents
            Node walker = node.parent;
            while (walker != null) {
                walker.nrOfChildrenAndThis -= 1;
                walker.sum -= node.index + 1;//index+1=rank
                walker = walker.parent;
            }
            node.parent.children.remove(node);
            //reset values
        }
        node.sum = node.index + 1;//index+1=rank
        node.nrOfChildrenAndThis = 1;
        //add children of node to parent
        //the values need not be added since we kept them when removing node
        for (Node child : node.children) {
            node.parent.children.add(child);
            child.parent = node.parent;
        }
        //remove node from set
        node.parent = null;
        node.children = new HashSet<>();
    }

    void move(final Node e, final Node to) {
        extractToEmptySet(e);
        union(e, to);
    }

    int countSum, countNr;

    void count(Node node) {
        node = getTop(node);
        countSum = node.sum;
        countNr = node.nrOfChildrenAndThis;
    }

    void innerCount(Node e) {
        countSum += e.index + 1;// index +1 = rank
        countNr++;
        for (Node child : e.children) innerCount(child);
    }


    Node getTop(final Node e) {
        List<Node> traveled = new LinkedList<>();
        Node traveler = e;
        while (traveler.parent != null) {
            traveled.add(e);
            traveler = traveler.parent;
        }
        for (Node node : traveled) {
            node.parent.children.remove(node);
            node.parent = traveler;
            traveler.children.add(node);
        }
        return traveler;
    }

    class Node {
        int index;
        Node parent;
        Set<Node> children = new HashSet<>();

        int nrOfChildrenAndThis;
        int sum;

        Node(int index) {
            this.index = index;
            this.nrOfChildrenAndThis = 1;
            this.sum = index + 1;//index+1 = rank
        }
    }

    void printSets() {
        Set<Node> topNodes = new HashSet<>();
        for (int i = 0; i < n; i++) {
            Node node = nodes[i];
            topNodes.add(getTop(node));
        }
        for (Node node : topNodes) {
            System.out.print("[");
            printWithBrackets(node, true);
            System.out.print("] ");
        }
        System.out.println();
    }

    void printWithoutBrackets(Node node, boolean isFirst) {
        if (!isFirst) System.out.print(", ");
        System.out.print(node.index + 1);
        for (Node child : node.children) printWithoutBrackets(child, false);
    }

    void printWithBrackets(Node node, boolean isFirst) {
        if (!isFirst) System.out.print(", ");
        System.out.print(node.index + 1);
        if (!node.children.isEmpty()) {
            System.out.print("[");
            isFirst = true;
            for (Node child : node.children) {
                printWithBrackets(child, isFirst);
                isFirst = false;
            }
            System.out.print("]");
        }
    }


    void count2(Node e) {
        countSum = 0;
        countNr = 0;
        Node top = getTop(e);
        innerCount(top);
    }


    Node getTop2(Node e) {
        if (e.parent == null) return e;
        Node top = getTop(e.parent);
        e.parent.children.remove(e);
        top.children.add(e);
        e.parent = top;
        return top;
    }
}
