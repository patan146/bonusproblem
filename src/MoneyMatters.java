import reader.Reader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * User: Patrik
 * Date: 2013-10-09
 * Time: 18:47
 */
public class MoneyMatters {
    public static void main(String[] args) throws IOException {
        new MoneyMatters().run();
    }

    int n;
    int m;
    Node[] nodes;

    private void run() throws IOException {
        Reader in = new Reader();
        int[] line = in.readInts();
        n = line[0];
        m = line[1];
        nodes = new Node[n];

        for (int i = 0; i < n; i++) {
            Node node = new Node(in.readInt());
            nodes[i] = node;
        }
        for (int i = 0; i < m; i++) {
             line = in.readInts();
            int n1 = line[0];
            int n2 = line[1];
            nodes[n1].friends.add(nodes[n2]);
            nodes[n2].friends.add(nodes[n1]);
        }

        //Algorithm
        for (Node node : nodes) {
            int sum = dfs(node);
            if (sum != 0) {
                System.out.println("IMPOSSIBLE");
                return;
            }
        }
        System.out.println("POSSIBLE");
    }

    private int dfs(Node node) {
        if (node.label == Label.VISITED) return 0;
        node.label = Label.VISITED;
        int sum = 0;
        for (Node friend: node.friends){
            sum += dfs(friend);
        }
        return sum + node.owed;
    }

    class Node {
        int owed;
        List<Node> friends = new LinkedList<>();
        Label label = Label.UNVISITED;

        Node( int owed) {
            this.owed = owed;
        }
    }
    enum Label{UNVISITED, VISITED}
}
