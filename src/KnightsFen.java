import graphKnightsFen.ChessBoard;
import graphKnightsFen.ChessPiece;
import reader.Reader;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 17/10/13 Time: 8:18 AM To change this template use File | Settings | File
 * Templates.
 */
public class KnightsFen
{
	static final int MAX_MOVES = 10;
	static final int BOARD_WIDTH = 5;
	static final int BOARD_HEIGHT = 5;
	static final ChessPiece[][] SOLVED_ARRAY;
	public static final ChessBoard SOLVED_BOARD;

	static {
		ChessPiece[] row1 =
				new ChessPiece[] { ChessPiece.Black, ChessPiece.Black, ChessPiece.Black, ChessPiece.Black, ChessPiece.Black };
		ChessPiece[] row2 =
				new ChessPiece[] { ChessPiece.White, ChessPiece.Black, ChessPiece.Black, 
				ChessPiece.Black, ChessPiece.Black };
		ChessPiece[] row3 =
				new ChessPiece[] { ChessPiece.White, ChessPiece.White, ChessPiece.Empty, ChessPiece.Black, ChessPiece.Black };
		ChessPiece[] row4 =
				new ChessPiece[] { ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.Black };
		ChessPiece[] row5 =
				new ChessPiece[] { ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.White };
		SOLVED_ARRAY = new ChessPiece[][] { row1, row2, row3, row4, row5 };
		SOLVED_BOARD = new ChessBoard(SOLVED_ARRAY, new int[]{2,2});
	}

	public static void main(String[] args) throws IOException {
		new KnightsFen().run();
	}

	private void run() throws IOException {
		Reader in = new Reader();
		int nrOfInputs = in.readInt();
		for (int boardNr = 0; boardNr < nrOfInputs; boardNr++) {
			//Input
			ChessPiece[][] board = new ChessPiece[BOARD_HEIGHT][BOARD_WIDTH];
			int[] emptyPos = null;
			for (int y = 0; y < board.length; y++) {
				ChessPiece[] row = board[y];
				String line = in.readString();
				for (int x = 0; x < row.length; x++) {
					char c = line.charAt(x);
					switch (c) {
					case ' ':
						row[x] = ChessPiece.Empty;
						emptyPos = new int[] { x, y };
						break;
					case '0':
						row[x] = ChessPiece.White;
						break;
					case '1':
						row[x] = ChessPiece.Black;
						break;
					}
				}
			}
			//Algorithm - Exhaustive Search
			int solution = BFS(new ChessBoard(board, emptyPos));
			if (solution <= 10) System.out.println(solution);
			else System.out.println("11<");
		}
	}

	int BFS(final ChessBoard start) {
		Queue<ChessBoard> adjacent = new LinkedList<>();
		Set<ChessBoard> visited = new HashSet<>();
		adjacent.add(start);

		int testcntr = 0;
		while (!adjacent.isEmpty()) {
			testcntr++;
			ChessBoard board = adjacent.poll();
			if (board.isSolved() || MAX_MOVES < board.moves) {
				return board.moves;
			}
			for (int i = 1; i <= 8; i++) {//8 possible moves
				int[] knightPosition = knightPosition(board.emptyPos, i);
				int x = knightPosition[0];
				int y = knightPosition[1];
				if (0 <= x && x < BOARD_WIDTH && 0 <= y && y < BOARD_HEIGHT) {
					board.swapEmpty(knightPosition);
					//TEST
					boolean inVisited = false;
					for (ChessBoard visb: visited){
						if (visb.equals(board)) inVisited=true;
					}
					//TEST
					if (!inVisited) {
						ChessBoard newBoard = new ChessBoard(board);
						visited.add(newBoard);
						adjacent.add(newBoard);
					}
					board.swapEmpty(knightPosition);
				    board.moves-=2;//ugly as hell
				}
			}
		}
		System.err.println(testcntr);
		throw  new UnsupportedOperationException("The problem is unsolvable.");
	}

	void swap(ChessPiece[][] board, int[] pos1, int[] pos2) {
		int pos1X = pos1[0];
		int pos1Y = pos1[1];
		int pos2X = pos2[0];
		int pos2Y = pos2[1];
		ChessPiece tmp = board[pos1Y][pos1X];
		board[pos1Y][pos1X] = board[pos2Y][pos2X];
		board[pos2Y][pos2X] = tmp;
	}

	int[] knightPosition(int[] position, int index) {
		int x = position[0];
		int y = position[1];
		switch (index) {
		/*
		 *  7   8 
		 * 6     1
		 *    S  
		 * 5     2
		 *  4   3
		 */
		case 1:
			return new int[] { x + (2), y + (-1) };
		case 2:
			return new int[] { x + (2), y + (1) };
		case 3:
			return new int[] { x + (1), y + (2) };
		case 4:
			return new int[] { x + (-1), y + (2) };
		case 5:
			return new int[] { x + (-2), y + (1) };
		case 6:
			return new int[] { x + (-2), y + (-1) };
		case 7:
			return new int[] { x + (-1), y + (-2) };
		case 8:
			return new int[] { x + (1), y + (-2) };
		default:
			throw new NoSuchElementException();
		}
	}

	String chessBoardToString(ChessPiece[][] board) {
		StringBuilder strbuild = new StringBuilder();
		for (int y = 0; y < board.length; y++) {
			ChessPiece[] row = board[y];
			for (int x = 0; x < row.length; x++) {
				ChessPiece chessPiece = row[x];
				switch (chessPiece) {
				case Empty:
					strbuild.append(' ');
					break;
				case White:
					strbuild.append('0');
					break;
				case Black:
					strbuild.append('1');
					break;
				}
			}
			strbuild.append('\n');
		}
		return strbuild.toString();
	}
	
	public static ChessPiece[][] copy(ChessPiece[][] arr){
		ChessPiece[][] newArr = new ChessPiece[arr.length][arr[0].length];
		for (int y = 0; y < arr.length; y++){
			for (int x=0; x < arr[y].length;x++){
				newArr[y][x] = arr[y][x];
			}
		}
		return newArr;
	}
}

class KnightPositions implements Iterator<int[]>
{
	private int atPos = 0;
	int emptyX;
	int emptyY;

	KnightPositions(final int[] emptyPos) {
		this.emptyX = emptyPos[0];
		this.emptyY = emptyPos[1];
	}

	@Override public boolean hasNext() {
		return atPos < 8;
	}

	@Override public int[] next() {
		atPos++;
		switch (atPos) {
		case 1:
			return new int[] { emptyX + (2), emptyY + (2) };
		case 2:
			return new int[] { emptyX + (2), emptyY + (1) };
		case 3:
			return new int[] { emptyX + (1), emptyY + (-2) };
		case 4:
			return new int[] { emptyX + (2), emptyY + (-1) };
		case 5:
			return new int[] { emptyX + (-1), emptyY + (-2) };
		case 6:
			return new int[] { emptyX + (-2), emptyY + (-1) };
		case 7:
			return new int[] { emptyX + (-1), emptyY + (-2) };
		case 8:
			return new int[] { emptyX + (-2), emptyY + (-1) };
		default:
			throw new NoSuchElementException();
		}
	}

	@Override public void remove() {throw new UnsupportedOperationException();}
}

/*
1
111 1
01111
00111
00001
00000


2
01011
110 1
01110
01010
00100
10110
01 11
10111
01001
00000
 */