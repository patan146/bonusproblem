package reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * User: Patrik
 * Date: 2013-10-05
 * Time: 18:38
 */
public class Reader {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public boolean ready() throws IOException {
        return in.ready();
    }

    public int readInt() throws IOException {
        return Integer.parseInt(in.readLine());
    }

    public int[] readInts() throws IOException {
        String[] words = readStrings();
        int[] ints = new int[words.length];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = Integer.parseInt(words[i]);
        }
        return ints;
    }

    public String readString() throws IOException {
        return in.readLine();
    }

    public String[] readStrings() throws IOException {
        return in.readLine().split(" ");
    }
    public String[] readStrings(String regex) throws IOException {
        return in.readLine().split(regex);
    }
}
