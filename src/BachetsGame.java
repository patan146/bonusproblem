import reader.Reader;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 20/10/13 Time: 12:08 PM To change this template use File | Settings | File
 * Templates.
 */
public class BachetsGame
{
    public static void main(String[] args) throws IOException {
	new BachetsGame().run();
    }

    private void run() throws IOException {
	Reader in = new Reader();
	do {
	    //Input
	    int[] line = in.readInts();
	    int nrOfStones = line[0];
	    int totalMoves = line[1];
	    int[] moves = new int[totalMoves];
	    for (int i = 0; i < totalMoves; i++) {
		moves[i] = line[i + 2];
	    }

	    //Processing

	    //true represents that it's Ollie's turn.
	    //false represents it being Stan's turn
	    boolean[] olliesTurnAtStonesTaken = new boolean[nrOfStones + 1];//there are scenarios 0 and 1..nrOfStones

	    for (int stone = 0; stone < olliesTurnAtStonesTaken.length; stone++) {
		boolean isOlliesTurn = olliesTurnAtStonesTaken[stone];
		if (!isOlliesTurn) {//if Stan's turn
		    for (int move : moves) {
			//Then there will be Ollie's turn at the following stones
			if (stone + move < olliesTurnAtStonesTaken.length)
			    olliesTurnAtStonesTaken[stone + move] = true;
		    }
		}
	    }
	    if (olliesTurnAtStonesTaken[nrOfStones])//if Ollie's turn when no stones
		System.out.println("Stan wins");
	    else
		System.out.println("Ollie wins");

	} while (in.ready());

    }
}
