
/**
 * User: Patrik
 * Date: 2013-09-20
 * Time: 19:03
 */
public class StrComp {
    public static void main(String[] args) {
        String correct =
                "DESCENDANTS of John Doe\n" +
                        "  Casper Ghost Jan 2, 1959 -\n" +
                        "    Child3 Children May 15, 1999 -\n" +
                        "      Child Children May 15, 1999 -\n" +
                        "      Child2 Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "    Child4 Children May 13, 1999 -\n" +
                        "      Child2 Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "  Girl Girlish Jan 2, 1959 -\n" +
                        "    Child3 Children May 15, 1999 -\n" +
                        "      Child Children May 15, 1999 -\n" +
                        "      Child2 Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "    Child4 Children May 13, 1999 -\n" +
                        "      Child2 Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "\n" +
                        "ANCESTORS of Child Children\n" +
                        "  Child2 Children May 13, 1999 -\n" +
                        "    Child3 Children May 15, 1999 -\n" +
                        "      Casper Ghost Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "      Girl Girlish Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "    Child4 Children May 13, 1999 -\n" +
                        "      Casper Ghost Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "      Girl Girlish Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "  Child3 Children May 15, 1999 -\n" +
                        "    Casper Ghost Jan 2, 1959 -\n" +
                        "      John Doe\n" +
                        "      Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "        Adam Doe\n" +
                        "        Jane Doe\n" +
                        "    Girl Girlish Jan 2, 1959 -\n" +
                        "      John Doe\n" +
                        "      Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "        Adam Doe\n" +
                        "        Jane Doe\n";
        String output =
                "ANCESTORS of Child Children\n" +
                        "  ChildA Children May 13, 1999 -\n" +
                        "    ChildB Children May 15, 1999 -\n" +
                        "      Casper Ghost Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "      Girl Girlish Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "    ChildC Children May 13, 1999 -\n" +
                        "      Casper Ghost Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "      Girl Girlish Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "  ChildB Children May 15, 1999 -\n" +
                        "    Casper Ghost Jan 2, 1959 -\n" +
                        "      John Doe\n" +
                        "      Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "        Adam Doe\n" +
                        "        Jane Doe\n" +
                        "    Girl Girlish Jan 2, 1959 -\n" +
                        "      John Doe\n" +
                        "      Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "        Adam Doe\n" +
                        "        Jane Doe\n" +
                        "\n" +
                        "DESCENDANTS of John Doe\n" +
                        "  Casper Ghost Jan 2, 1959 -\n" +
                        "    ChildB Children May 15, 1999 -\n" +
                        "      Child Children May 15, 1999 -\n" +
                        "      ChildA Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "    ChildC Children May 13, 1999 -\n" +
                        "      ChildA Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "  Girl Girlish Jan 2, 1959 -\n" +
                        "    ChildB Children May 15, 1999 -\n" +
                        "      Child Children May 15, 1999 -\n" +
                        "      ChildA Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "    ChildC Children May 13, 1999 -\n" +
                        "      ChildA Children May 13, 1999 -\n" +
                        "        Child Children May 15, 1999 -\n" +
                        "\n" +
                        "ANCESTORS of Child Children\n" +
                        "  ChildA Children May 13, 1999 -\n" +
                        "    ChildB Children May 15, 1999 -\n" +
                        "      Casper Ghost Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "      Girl Girlish Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "    ChildC Children May 13, 1999 -\n" +
                        "      Casper Ghost Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "      Girl Girlish Jan 2, 1959 -\n" +
                        "        John Doe\n" +
                        "        Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "          Adam Doe\n" +
                        "          Jane Doe\n" +
                        "  ChildB Children May 15, 1999 -\n" +
                        "    Casper Ghost Jan 2, 1959 -\n" +
                        "      John Doe\n" +
                        "      Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "        Adam Doe\n" +
                        "        Jane Doe\n" +
                        "    Girl Girlish Jan 2, 1959 -\n" +
                        "      John Doe\n" +
                        "      Mary Worth May 18, 1666 - Jan 3, 1959\n" +
                        "        Adam Doe\n" +
                        "        Jane Doe\n" +
                        "\n";

        System.out.println(correct.equals(output));
    }
}
