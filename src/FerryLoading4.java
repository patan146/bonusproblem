import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * User: Patrik
 * Date: 2013-09-10
 * Time: 23:22
 */
public class FerryLoading4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int c = in.nextInt();
        for (int caseNr = 0; caseNr < c; caseNr++) {
            String currentBank = "left";
            int crosses = 0;
            int currentLength = 0;
            int maxLength = in.nextInt() * 100;
            int m = in.nextInt();
            Queue<Integer> qLeft = new LinkedList<Integer>();
            Queue<Integer> qRight = new LinkedList<Integer>();

            for (int carNr = 0; carNr < m; carNr++) {
                int carLength = in.nextInt();
                String bank = in.nextLine();

                if (bank.equals(" left")) qLeft.add(carLength);
                else qRight.add(carLength);
            }

            while (!(qLeft.isEmpty() && qRight.isEmpty())) {
                if (currentBank.equals("left")) {
                    if (!qLeft.isEmpty() && (currentLength + qLeft.peek() <= maxLength)) {
                        currentLength += qLeft.poll();
                    } else {
                        currentBank = "right";
                        crosses++;
                        currentLength = 0;
                    }
                } else {
                    if (!qRight.isEmpty() && (currentLength + qRight.peek() <= maxLength)) {
                        currentLength += qRight.poll();
                    } else {
                        currentBank = "left";
                        crosses++;
                        currentLength = 0;
                    }
                }
            }
            System.out.println(++crosses);
        }
    }
}
