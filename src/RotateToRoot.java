import reader.Reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Patrik
 * Date: 2013-09-28
 * Time: 22:52
 */
public class RotateToRoot {
    public static void main(String[] args) throws IOException {
        new RotateToRoot().run();
    }

    List<Node> nodes;

    private void run() throws IOException {
        Reader in = new Reader();
        int n = in.readInt();
        if (n == 0) System.exit(0);
        nodes = new ArrayList<Node>(n);
        for (int i = 0; i < n; i++) nodes.add(new Node());

        for (int i = 0; i < n; i++) {
            int[] children = in.readInts();
            Node node = nodes.get(i);
            node.leftChild = children[0];
            nodes.get(node.leftChild).parent = i;
            node.rightChild = children[1];
            nodes.get(node.rightChild).parent = i;
        }

        Node root = null;
        for (Node node: nodes) if (node.parent==-1) root=node;

        //Algorithm
        solve(root);

    }

    void solve(Node node){
        //bottom up
        if (node.leftChild!=-1) solve(nodes.get(node.leftChild));
        if (node.rightChild!=-1) solve(nodes.get(node.rightChild));

    }

    class Node {
        int nr;
        int parent = -1;
        int leftChild = -1, rightChild = -1;
        int height;
    }
}
