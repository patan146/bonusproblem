import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * User: Patrik
 * Date: 2013-09-17
 * Time: 18:04
 */
public class Genealogical {
    public static void main(String[] args) throws IOException {
            new Genealogical().main2();
    }

    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    Map<String, Person> map = new HashMap<String, Person>();

    public void main2() throws IOException {
        int command = 0;
        int person = 1;
        int date = 2;
        int mother = 3;
        int father = 4;
        boolean first = true;
        do {
            String[] arr = getInput();
            if (arr[command].equals("BIRTH")) {
                addBirth(arr[person], arr[date], arr[mother], arr[father]);
            } else if (arr[command].equals("DEATH")) {
                addDeath(arr[person], arr[date]);
            } else if (arr[command].equals("ANCESTORS")) {
                if (first) first = false;
                else System.out.println();
                ancestors(arr[person]);
            } else if (arr[command].equals("DESCENDANTS")){
                if (first) first = false;
                else System.out.println();
                descendants(arr[person]);
            } else if (arr[command].equals("QUIT")) {
                break;
            }
        } while (in.ready());
    }

    String[] getInput() throws IOException {
        String line = in.readLine();
        String[] byColon = line.split(":");
        for (int i = 0; i < byColon.length; i++) {
            byColon[i] = byColon[i].trim();
        }
        String command = byColon[0].split(" ")[0];
        String name = byColon[0].substring(byColon[0].indexOf(' ') + 1);
        String[] retval = new String[byColon.length + 1];
        int i = 0;
        retval[i++] = command;
        retval[i++] = name;
        for (int j = i - 1; j < byColon.length; j++) {
            retval[i++] = byColon[j];
        }
        return retval;
    }

    void addBirth(String person, String date, String mother, String father) {
        if (!map.containsKey(person)) map.put(person, new Person(person));
        if (!map.containsKey(father)) map.put(father, new Person(father));//changed so that it uses existing parents
        if (!map.containsKey(mother)) map.put(mother, new Person(mother));
        Person personObj = map.get(person);
        personObj.birth = date;
        personObj.parents.add(map.get(father));
        personObj.parents.add(map.get(mother));
        addChild(mother, person);
        addChild(father, person);
    }

    void addDeath(String person, String death) {
        if (!map.containsKey(person)) map.put(person, new Person(person));
        map.get(person).death = death;
    }

    void addChild(String person, String child) {
        if (!map.containsKey(person)) map.put(person, new Person(person));
        map.get(person).children.add(map.get(child));
    }

    void ancestors(String person) {
        SortedSet<Person> relatives = map.get(person).parents;
        int n = 0;//indentation level
        System.out.println("ANCESTORS of " + person);
        n += 2;
        for (Person relative : relatives) relatives(relative.toString(), n, true);
        //System.out.println();
    }
    void descendants(String person) {
        SortedSet<Person> relatives = map.get(person).children;
        int n = 0;//indentation level
        System.out.println("DESCENDANTS of " + person);
        n += 2;
        for (Person relative : relatives) relatives(relative.toString(), n, false);
        //System.out.println();
    }

    void relatives(String person, int n, boolean ancestors) {
        String birth = map.get(person).birth;
        String death = map.get(person).death;
        SortedSet<Person> relatives;
        if (ancestors) relatives = map.get(person).parents;
        else relatives = map.get(person).children;

        ind(n);
        System.out.print(person);
        if (birth != null) System.out.print(" " + birth + " -");
        if (death != null) System.out.print(" "+ death);
        System.out.println();
        n += 2;
        for (Person relative : relatives) relatives(relative.toString(), n, ancestors);
    }

        class Person implements Comparable {
            String name;
            String birth = null;
            String death = null;
            SortedSet<Person> parents = new TreeSet<Person>();
            SortedSet<Person> children = new TreeSet<Person>();

            Person(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return name;
            }

            @Override
            public int compareTo(Object o) {
                return this.name.compareTo(((Person) o).name);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Person)) return false;

                Person other = (Person) o;

                return name.equals(other.name);

            }

            @Override
            public int hashCode() {
                return name.hashCode();
            }
        }

    //--------------formatting--------------

    void ind(int n) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n; i++) s.append(' ');
        System.out.print(s.toString());
    }
    //--------------------------------------
}
/*
###INPUT###
BIRTH John Doe : January 7, 1599 : Jane Doe : Adam Doe
BIRTH Mary Worth : May 18, 1666 : Jane Doe : Adam Doe
BIRTH Casper Ghost : Jan 2, 1959 : Mary Worth : John Doe
DEATH Mary Worth : Jan 3, 1959
ANCESTORS Casper Ghost
DESCENDANTS John Doe
QUIT

###OUTPUT###
ANCESTORS of Casper Ghost
  John Doe January 7, 1599 -
    Adam Doe
    Jane Doe
  Mary Worth May 18, 1666 - Jan 3, 1959
    Adam Doe
    Jane Doe

DESCENDANTS of John Doe
  Casper Ghost Jan 2, 1959 -

###INPUT###
BIRTH Dddd :  January 7, 1599 : xxx : yyy
BIRTH Bbbb :  January 7, 1599 : xxx : yyy
BIRTH Aaaa :  January 7, 1599 : xxx : yyy
BIRTH Cccc :  January 7, 1599 : xxx : yyy
DESCENDANTS xxx
QUIT
*/