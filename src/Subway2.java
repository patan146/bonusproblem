/**
 * User: Patrik
 * Date: 2013-10-14
 * Time: 21:04
 */

import reader.Reader;

import java.io.IOException;
import java.util.*;

/**
 * Time unit: seconds
 * Distance unit: meters
 */
public class Subway2 {
    public static void main(String[] args) throws IOException {
        new Subway2().run();
    }

    static int walkKMPH = 10;
    static int trainKMPH = 40;
    static double WalkMPS = walkKMPH * 1000.0 / 3600;
    static double TrainMPS = trainKMPH * 1000.0 / 3600;

    Map<int[], Node> nodes = new HashMap<>();

    void run() throws IOException {
        Reader in = new Reader();
        int[] line = in.readInts();
        int startX = line[0];
        int startY = line[1];
        int schoolX = line[2];
        int schoolY = line[3];
        Node start = nodeAt(startX, startY);
        Node school = nodeAt(schoolX, schoolY);

        //Train edges
        while (in.ready()) {
            line = in.readInts();
            for (int i = 2; i < line.length - 2; i += 2) {//Last on row are "-1 -1": length-2
                int fromX = line[i - 2];
                int fromY = line[i - 1];
                int toX = line[i];
                int toY = line[i + 1];
                Node from = nodeAt(fromX, fromY);
                Node to = nodeAt(toX, toY);
                Edge e = new Edge(to, from, true);
                from.edges.add(e);
                to.edges.add(e);
            }
        }
        //Walk edges
        //Currently creates walk edge loops
        int outerCount = -1;
        for (Node outer : nodes.values()) {
            outerCount++;
            Node inner;
            Iterator<Node> innerIt = nodes.values().iterator();
            for (int i = 0; i <= outerCount; i++) {
                inner = innerIt.next();
            }
            while (innerIt.hasNext()) {
                inner = innerIt.next();
                Edge e = new Edge(outer, inner, false);
                outer.edges.add(e);
                inner.edges.add(e);
            }
        }

        //Dijkstra's shortest path
        start.minTime = 0;
        Queue<Node> closeNodes = new PriorityQueue<>();

        closeNodes.add(start);
        //We should find school and break before we visit all nodes
        while (true) {
            //Cloudify closest
            Node closest = closeNodes.remove();
            closest.cloudify();
            //If school was added to cloud
            if (closest==school) {
                answer(closest.minTime);
                break;
            }
            //Relax edges if appropriate
            for (Edge edge : closest.edges) {
                final Node adjacentToClosest = edge.otherSide(closest);
                double timeFromHere = closest.minTime + edge.weight;
                if (timeFromHere < adjacentToClosest.minTime) {
                    adjacentToClosest.minTime = timeFromHere;
                    closeNodes.remove(adjacentToClosest);
                    closeNodes.add(adjacentToClosest);
                }
                //Add nodes adjacent to closest to closeNodes
                if (!adjacentToClosest.isInCloud() && !closeNodes.contains(adjacentToClosest)) closeNodes.add(adjacentToClosest);
            }
        }
    }

    private void answer(double minTime) {
        System.out.println(Math.round(minTime/60));
    }

    Node nodeAt(int x, int y) {
        final int[] key = {x, y};
        Node retval = nodes.get(key);
        if (retval == null) {
            retval = new Node(x, y);
            nodes.put(key, retval);
        }
        return retval;
    }
}
class Node implements Comparable<Node> {
    boolean visited;
    int x;
    int y;
    double minTime = Double.POSITIVE_INFINITY;
    PriorityQueue<Edge> edges = new PriorityQueue<>();

    Node(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;

        Node node = (Node) o;

        if (x != node.x) return false;
        if (y != node.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public int compareTo(Node o) {
        return Double.compare(this.minTime, o.minTime);
    }

    public void cloudify() {
        this.visited = true;
    }

    public boolean isInCloud(){
        return this.visited;
    }
}

class Edge implements Comparable<Edge> {
    Node node1;
    Node node2;
    double weight;

    Edge(Node node1, Node node2, boolean byTrain) {
        this.node1 = node1;
        this.node2 = node2;
        this.weight = Math.sqrt(Math.pow(node1.x - node2.x, 2) + Math.pow(node1.y - node2.y, 2));
        this.weight /= byTrain ? Subway2.TrainMPS : Subway2.WalkMPS;
    }

    Node otherSide(Node start) {
        if (this.node1 == start) return node2;
        else return node1;
    }

    @Override
    public int compareTo(Edge o) {
        return Double.compare(this.weight, o.weight);
    }
}

/*
Test case:
0 0 10000 1000
0 200 5000 200 7000 200 -1 -1
2000 600 5000 600 10000 600 -1 -1
Correct Output:
21
*/