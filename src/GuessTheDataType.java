import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * User: Patrik
 * Date: 2013-09-28
 * Time: 00:46
 */
public class GuessTheDataType {
    public static void main(String[] args) throws IOException {
        new GuessTheDataType().run();
    }

    public void run() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (in.ready()) {
            int n = Integer.parseInt(in.readLine());

            Queue<Integer> queue = new LinkedList<Integer>();
            Stack<Integer> stack = new Stack<Integer>();
            PriorityQueue<Integer> priorityQueue = new PriorityQueue<Integer>(n, new invComparator());//comparator?

            boolean isQueue = true, isStack = true, isPriorityQueue = true;

            for (int i = 0; i < n; i++) {
                //input
                String[] line = in.readLine().split(" ");
                int type = Integer.parseInt(line[0]);
                int x = Integer.parseInt(line[1]);
                //Algorithm
                switch (type) {
                    case 1:
                        queue.add(x);
                        stack.add(x);
                        priorityQueue.add(x);
                        break;
                    case 2:
                        Integer queueOut = queue.poll();
                        Integer stackOut = stack.isEmpty() ? null : stack.pop();
                        Integer pQueueOut = priorityQueue.poll();
                        if (queueOut == null) isQueue = isStack = isPriorityQueue = false;
                        else{
                            if (queueOut != x) isQueue = false;
                            if (stackOut != x) isStack = false;
                            if (pQueueOut != x) isPriorityQueue = false;
                        }
                }
            }
            //Answer
            int p = (isQueue ? 1 : 0) + (isStack ? 1 : 0) + (isPriorityQueue ? 1 : 0);
            if (p < 1) System.out.println("impossible");
            else if (p == 1) {
                if (isQueue) System.out.println("queue");
                else if (isStack) System.out.println("stack");
                else System.out.println("priority queue");
            } else System.out.println("not sure");
        }
    }

    public class invComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return -(o1.compareTo(o2));
        }
    }
}

