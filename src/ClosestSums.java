import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * User: Patrik
 * Date: 2013-09-14
 * Time: 18:15
 */
public class ClosestSums {
    static Kattio io = new Kattio(System.in);

    public int next() {
        return io.getInt();
    }

    public void answer(int closest, int query) {
        io.println("Closest sum to " + query + " is " + closest + ".");
    }

    public int findClosest(ConcurrentSkipListSet<Integer> skipSet, int e) {
        //floor finds closest int smaller or equal to e.
        //with O(log(n))
        Integer under = skipSet.floor(e);
        Integer over = skipSet.ceiling(e);
        if (under == null) return over;
        if (over == null) return under;
        if (Math.abs(over - e) < Math.abs(under - e)) return over;
        else return under;
    }

    public static void main(String[] args) throws IOException {
        int caseNr = 0;
        while (io.hasMoreTokens()) {
            io.println("Case " + ++caseNr + ":");
            new ClosestSums().hard();
            io.flush();
        }
        io.close();
    }

    public void easy() {
        int nrOfSummables = next();
        ConcurrentSkipListSet<Integer> starters = new ConcurrentSkipListSet<Integer>();
        ConcurrentSkipListSet<Integer> sums = new ConcurrentSkipListSet<Integer>();
        for (int sumNr = 0; sumNr < nrOfSummables; sumNr++) {
            starters.add(next());
        }
        for (int x : starters) {
            for (int y : starters) {
                if (x != y) sums.add(x + y);
            }
        }
        int nrOfQueries = next();

        for (int queryNr = 0; queryNr < nrOfQueries; queryNr++) {
            int query = next();
            answer(findClosest(sums, query), query);
        }
    }

    public void hard() {
        int nrOfSummables = next();
        ConcurrentSkipListSet<Integer> unSummed = new ConcurrentSkipListSet<Integer>();
        ConcurrentSkipListSet<Integer> sums = new ConcurrentSkipListSet<Integer>();
        for (int sumNr = 0; sumNr < nrOfSummables; sumNr++) {
            unSummed.add(next());
        }

        int nrOfQueries = next();
        int queryCounter = -1;
        Iterator<Integer> ity;
        Iterator<Integer> itx;
        while (++queryCounter < nrOfQueries) {
            int query = next();

            //Makes all sums smaller than or equal to query.
            //Also makes at least one countSum bigger or equal(if possible).
            //Does not make sums that has already been made.
            //Makes all the sums for at least one unSummed(if there is one).
            while (!unSummed.isEmpty()) {
                int secondFirst = 0;//value irrelevant
                ity = unSummed.iterator();
                Integer y = ity.next();
                itx = unSummed.iterator();
                itx.next();
                if (itx.hasNext()) {
                    int x = itx.next();
                    secondFirst = x;

                    while (true) {
                        sums.add(y + x);
                        if (itx.hasNext()) x = itx.next();
                        else break;
                    }
                }
                ity.remove();
                if (query <= y + secondFirst) break;
                //if (y+last <= query) break;
            }
            //answers with O(log(n))
            answer(findClosest(sums, query), query);
        }
    }

    public void arrays() {
        int n = io.getInt();
        int[] N = new int[n];

        for (int i = 0; i < N.length; i++) {
            N[i] = io.getInt();
        }

        int[] sums = new int[n * (n - 1) / 2];
        int c = 0;
        for (int i = 0; i < N.length - 1; i++) {
            for (int j = i + 1; j < N.length; j++) {
                sums[c++] = N[i] + N[j];
            }
        }

        Arrays.sort(sums);
        int m = io.getInt();
        for (int i = 0; i < m; i++) {
            int query = io.getInt();
            //binary search with floor
            int first = 0;
            int last = sums.length - 1;
            int middle=0;
            boolean changed = true;
            while (first != (middle = (first+last)/2)) {
                if (sums[middle] < query) first = middle;
                else last = middle;
            }
            int dif1 = Math.abs(sums[middle]-query);
            int dif2 = Math.abs(sums[middle+1]-query);
            if (dif1 < dif2) answer(sums[middle],query);
            else answer(sums[middle+1],query);
        }
    }
}
