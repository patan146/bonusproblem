import reader.Reader;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

/**
 * User: Patrik
 * Date: 2013-10-08
 * Time: 19:56
 */
public class TourDeFrance {
    public static void main(String[] args) throws IOException {
        new TourDeFrance().run();
    }

    public void run() throws IOException {
        Reader in = new Reader();
        while (true) {
            int[] fr = in.readInts();
            int f = fr[0];
            if (f==0) return;
            int r = fr[1];
            int[] fs = in.readInts();
            int[] rs = in.readInts();

            Double[] ds = new Double[f*r];
            for (int fi = 0; fi < f; fi++) {
                for (int ri = 0; ri < r; ri++) {
                    ds[fi*r+ri] = (double)rs[ri]/(double)fs[fi];
                }
            }
            Arrays.sort(ds);

            Double dMax = 0.0;
            for (int i = 0; i < ds.length-1; i++) {
                double d1 = ds[i];
                double d2 = ds[i+1];
                if (d2/d1 > dMax)  dMax = d2/d1;
            }
            System.out.println(BigDecimal.valueOf(dMax).setScale(2, RoundingMode.HALF_EVEN));
        }
    }
}
