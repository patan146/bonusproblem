import reader.Reader;

import java.io.IOException;
import java.util.Arrays;

/**
 * User: Patrik
 * Date: 2013-10-08
 * Time: 22:57
 */
public class FroshWeek {
    public static void main(String[] args) throws IOException {
        new FroshWeek().run();
    }

    Student[] students;
    int swapCount = 0;

    private void run() throws IOException {
        Reader in = new Reader();
        int n = in.readInt();
        students = new Student[n];
        for (int i = 0; i < n; i++) students[i] = new Student(in.readInt(), i);

        //Analysis of start and target positions.
        Arrays.sort(students);

        int chaos=0;
        for (int currPos = 0; currPos < students.length; currPos++) {
            int origPos = students[currPos].origPos;
            chaos += Math.abs(origPos - currPos);
        }
        System.err.println(chaos);
        System.out.println(chaos/2);
    }
   /*
   5 4 3 2 1

   1 5 4 3 2 : 4
   1 2 5 4 3 : 3

   5 3 4 2 1
   5 3 2 4 1
   3 5 2 4 1
   ...
    */
    class Student implements Comparable<Student> {
        Integer val;
        int origPos;

        Student(Integer val, int origPos) {
            this.val = val;
            this.origPos = origPos;
        }

        @Override
        public int compareTo(Student o) {
            return val.compareTo(o.val);
        }
    }

    void swap(int x, int y) {
        Student tmp = students[x];
        students[x] = students[y];
        students[y] = tmp;
    }
}
