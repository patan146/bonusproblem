package graphKnightsFen;

import graph.Edge;
import graph.Node;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 11:57 AM To change this template use File | Settings | File
 * Templates.
 */
public class ChessEdge implements Edge<ChessBoard>
{
    ChessNode to;
    double weight = 1;

    public ChessEdge(final ChessNode to) {
	this.to = to;
    }

    @Override public Node<ChessBoard> to() {
	return to;
    }

    @Override public double weight() {
	return weight;
    }
}
