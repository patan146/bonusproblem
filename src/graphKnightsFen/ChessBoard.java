package graphKnightsFen;

import java.util.Arrays;

public class ChessBoard
{
    ChessPiece[] array;
    int width;
    public int[] emptyPos;
    public int moves = 100;//inf

    public ChessBoard(final ChessPiece[][] array, final int[] emptyPos) {
	this.width = array.length;
	this.array = oneDimArr(array);
	this.emptyPos = emptyPos;
    }

    public ChessBoard(ChessBoard other) {
	this.width = other.width;
	this.array = Arrays.copyOf(other.array, other.array.length);
	this.emptyPos = Arrays.copyOf(other.emptyPos, other.emptyPos.length);
    }

    private ChessPiece[] oneDimArr(final ChessPiece[][] array) {
	ChessPiece[] retarr = new ChessPiece[width*width];
	for (int y = 0; y < width; y++) {
	    ChessPiece[] row = array[y];
	    for (int x = 0; x < row.length; x++) {
		width = row.length;
		retarr[y* width +x] = array[y][x];
	    }
	}
	return retarr;
    }

    public boolean isSolved() {
	return this.equals(Main.SOLVED_BOARD);
    }

    public void swapEmpty(int[] pos) {
	moves++;
	swap(emptyPos, pos);
	emptyPos = Arrays.copyOf(pos, pos.length);
    }

    private void swap(int[] a, int[] b) {
	int ax = a[0];
	int ay = a[1];
	int bx = b[0];
	int by = b[1];
	ChessPiece tmp = array[ay*width+ax];
	array[ay*width+ax] = array[by*width+bx];
	array[by*width+bx] = tmp;
    }

    public static ChessPiece[][] copyArr(ChessPiece[][] arr) {
	ChessPiece[][] newArr = new ChessPiece[arr.length][arr[0].length];
	for (int y = 0; y < arr.length; y++) {
	    for (int x = 0; x < arr[y].length; x++) {
		newArr[y][x] = arr[y][x];
	    }
	}
	return newArr;
    }

    @Override
    public boolean equals(final Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;

	final ChessBoard that = (ChessBoard) o;

	if (!Arrays.equals(emptyPos, that.emptyPos)) return false;
	if (!Arrays.deepEquals(array, that.array)) return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result = array != null ? Arrays.deepHashCode(array) : 0;
	result = 31 * result + (emptyPos != null ? Arrays.hashCode(emptyPos) : 0);
	return result;
    }

    @Override public String toString() {
	StringBuilder strbuild = new StringBuilder();
	for (int y = 0; y < width; y++) {
	    //ChessPiece[] row = array[y];
	    for (int x = 0; x < width; x++) {
		ChessPiece chessPiece = array[y*width+x];
		switch (chessPiece) {
		    case Empty:
			strbuild.append(' ');
			break;
		    case White:
			strbuild.append('0');
			break;
		    case Black:
			strbuild.append('1');
			break;
		}
	    }
	    strbuild.append('\n');
	}
	return strbuild.toString();
    }
}
