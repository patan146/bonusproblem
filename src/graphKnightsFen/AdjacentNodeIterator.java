package graphKnightsFen;

import graph.Edge;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 4:27 PM To change this template use File | Settings | File
 * Templates.
 */
class AdjacentNodeIterator implements Iterator<Edge<ChessBoard>>
{
    int i;
    ChessBoard board;

    AdjacentNodeIterator(ChessBoard board) {
	this.board = board;
	i = 0;
	goToNextPossiblePos();
    }

    @Override public boolean hasNext() {
	return i <=8;
    }

    @Override public ChessEdge next() {
	int[] currentMove = knightPosition(board.emptyPos, i);
	goToNextPossiblePos();
	ChessBoard retBoard = new ChessBoard(board);
	retBoard.swapEmpty(currentMove);
	return new ChessEdge(ChessNode.get(retBoard));
    }

    @Override public void remove() {
	throw  new UnsupportedOperationException("Removal makes no sense in this context.");
    }

    private void goToNextPossiblePos() {
	i++;
	while (i <= 8) {
	    int[] knightPosition = knightPosition(board.emptyPos, i);
	    int x = knightPosition[0];
	    int y = knightPosition[1];
	    //if inside Board
	    if (0 <= x && x < 5 && 0 <= y && y < 5) {
		break;
	    }
	    i++;
	}
    }

    private int[] knightPosition(int[] position, int index) {
	int x = position[0];
	int y = position[1];
	switch (index) {
   		/*
   		 *   7    8
   		 * 6       1
   		 *     S
   		 * 5       2
   		 *   4   3
   		 */
	    case 1:
		return new int[] { x + (2), y + (-1) };
	    case 2:
		return new int[] { x + (2), y + (1) };
	    case 3:
		return new int[] { x + (1), y + (2) };
	    case 4:
		return new int[] { x + (-1), y + (2) };
	    case 5:
		return new int[] { x + (-2), y + (1) };
	    case 6:
		return new int[] { x + (-2), y + (-1) };
	    case 7:
		return new int[] { x + (-1), y + (-2) };
	    case 8:
		return new int[] { x + (1), y + (-2) };
	    default:
		throw new NoSuchElementException();
	}
    }
}
