package graphKnightsFen;

import graph.Edge;
import graph.Node;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 11:42 AM To change this template use File | Settings | File
 * Templates.
 */
public class ChessNode extends Node<ChessBoard> implements Iterable<Edge<ChessBoard>>
{
    public static Map<ChessBoard,ChessNode> NODES = new HashMap<>();

    public final ChessBoard board;

    static ChessNode get(final ChessBoard board){
	if (NODES.containsKey(board)) return NODES.get(board);
	else{
	    final ChessNode node = new ChessNode(board);
	    NODES.put(board,node);
	    return node;
	}
    }

    public int diffs(ChessNode other){
	int diffs = 0;
	ChessPiece[] arr1 = this.board.array;
	ChessPiece[] arr2 = other.board.array;
	int width = this.board.width;
	for (int y = 0; y < width; y++) {
	    for (int x = 0; x < width; x++) {
		if (!arr1[y*width+x].equals(arr2[y*width+x])) diffs++;
	    }
	}
	return diffs;
    }

    private ChessNode(final ChessBoard board) {this.board = board;}


    public Iterable<Edge<ChessBoard>> adjacent() {
	return this;
    }

    @Override public Iterator<Edge<ChessBoard>> iterator() {
	return new AdjacentNodeIterator(this.board);
    }

    public static void resetNodes() {
	NODES = new HashMap<>();
    }
}

