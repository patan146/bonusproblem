package graphKnightsFen;

import graph.Dijkstras;
import reader.Reader;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 12:16 PM To change this template use File | Settings | File
 * Templates.
 */
public class Main
{
    static final int MAX_MOVES = 10;
    static final int BOARD_WIDTH = 5;
    static final int BOARD_HEIGHT = 5;
    static final ChessPiece[][] SOLVED_ARRAY;
    public static final ChessBoard SOLVED_BOARD;

    static {
	ChessPiece[] row1 =
		new ChessPiece[] { ChessPiece.Black, ChessPiece.Black, ChessPiece.Black, ChessPiece.Black, ChessPiece.Black };
	ChessPiece[] row2 =
		new ChessPiece[] { ChessPiece.White, ChessPiece.Black, ChessPiece.Black, ChessPiece.Black, ChessPiece.Black };
	ChessPiece[] row3 =
		new ChessPiece[] { ChessPiece.White, ChessPiece.White, ChessPiece.Empty, ChessPiece.Black, ChessPiece.Black };
	ChessPiece[] row4 =
		new ChessPiece[] { ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.Black };
	ChessPiece[] row5 =
		new ChessPiece[] { ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.White, ChessPiece.White };
	SOLVED_ARRAY = new ChessPiece[][] { row1, row2, row3, row4, row5 };
	SOLVED_BOARD = new ChessBoard(SOLVED_ARRAY, new int[] { 2, 2 });
    }

    public static void main(String[] args) throws IOException {
	new Main().run();
    }

    private void run() throws IOException {
	Reader in = new Reader();
	int nrOfInputs = in.readInt();
	for (int boardNr = 0; boardNr < nrOfInputs; boardNr++) {
	    //Input
	    ChessPiece[][] array = new ChessPiece[BOARD_HEIGHT][BOARD_WIDTH];
	    int[] emptyPos = null;
	    for (int y = 0; y < array.length; y++) {
		ChessPiece[] row = array[y];
		String line = in.readString();
		for (int x = 0; x < row.length; x++) {
		    char c = line.charAt(x);
		    switch (c) {
			case ' ':
			    row[x] = ChessPiece.Empty;
			    emptyPos = new int[] { x, y };
			    break;
			case '0':
			    row[x] = ChessPiece.White;
			    break;
			case '1':
			    row[x] = ChessPiece.Black;
			    break;
		    }
		}
	    }
	    //Algorithm - Exhaustive Search
	    final ChessBoard board = new ChessBoard(array, emptyPos);
	    board.toString();
	    ChessNode.resetNodes();
	    final ChessNode start = ChessNode.get(board);
	    final ChessNode goal = ChessNode.get(SOLVED_BOARD);
	    Dijkstras.calcDists(start, goal);
	    double solution = goal.getDist();
	    System.out.println(solution);
	    //if (solution <= 10) System.out.println(solution);
	    //else System.out.println("11<");
	}
    }
}
/*
2
01011
110 1
01110
01010
00100
10110
01 11
10111
01001
00000

Q:
1
11111
01111
00011
 0001
00000
A:
1

Q:
1
11111
00111
00011
 1001
00000
A:
5
 */
