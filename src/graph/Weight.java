package graph;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 10:56 AM To change this template use File | Settings | File
 * Templates.
 */
public interface Weight extends Comparable<Weight>//<? super Weight>
{
    Weight zero();
    Weight add(Weight weight);
}
