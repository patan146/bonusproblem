package graph;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 10:41 AM To change this template use File | Settings | File
 * Templates.
 */
public interface Edge<V>
{
    Node<V> to();
    double weight();
}
