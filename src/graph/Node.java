package graph;

import java.security.InvalidParameterException;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 10:39 AM To change this template use File | Settings | File
 * Templates.
 */
public abstract class Node<V> implements Comparable<Node<V>>
{
    private double dist = Double.POSITIVE_INFINITY;
    protected Node<V> prev;

    public double getDist() {
	return dist;
    }
    public void setDist(final double dist) {
	if (dist < 0) throw new InvalidParameterException("Negative distance: "+dist);
	this.dist = dist;
    }
    public Node<V> getPrev() {
	return prev;
    }
    public void setPrev(final Node<V> prev) {
	this.prev = prev;
    }

    abstract public Iterable<Edge<V>> adjacent();


    @Override public int compareTo(final Node<V> o) {
	return ((Double)this.getDist()).compareTo(o.getDist());
    }
}


