package graph;

import graphKnightsFen.ChessNode;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA. User: patan146 Date: 19/10/13 Time: 10:40 AM To change this template use File | Settings | File
 * Templates.
 */
public final class Dijkstras
{
    private Dijkstras() {}

    /**
     * Calculates shortest distances between all nodes and start.
     * Quits when shortest distance for endNode is reached.
     * @param start Origin, will have distance 0.
     * @param endNode Stop node. null if all nodes are to be calculated.
     * @param <V> Value contained in Node.
     */
    public static <V> void calcDists(Node<V> start, Node<V> endNode) {
	start.setDist(0.0);
	PriorityQueue<Node<V>> closeNodes = new PriorityQueue<>();
	closeNodes.add(start);

	while (!closeNodes.isEmpty()) {
	    Node<V> closest = closeNodes.poll();
	    final Double closestDist = closest.getDist();

	    ChessNode chessNode = (ChessNode) closest;

	    if (ChessNode.NODES.containsKey(chessNode.board)){
		//System.err.println("repeated node, totalNodes: " + ChessNode.NODES.size());
	    }
	    if (closestDist.compareTo(10.0) > 0){
		break;
	    }
	    final int diffs = chessNode.diffs((ChessNode) endNode);
	    //if we have the solved board
	    if (diffs==0) break;
	    //if the number of moves we can make are less than
	    //the moves required to move diff pieces
	    else if (diffs - 2 > 10 - closestDist) break;

	    for (Edge<V> edge : closest.adjacent()) {
		Node<V> v = edge.to();
		double weight = edge.weight();
		Double thisDist = closest.getDist() + weight;
		if (thisDist.compareTo(v.getDist()) < 0) {
		    //Remove if already in queue
		    closeNodes.remove(v);
		    //Update distance, set the path variable
		    v.setDist(thisDist);
		    v.setPrev(closest);
		    //Add it to nodes to search
		    closeNodes.add(v);
		}
	    }
	}
    }

}
